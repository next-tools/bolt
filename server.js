var http = require('http');
var fs = require('fs');

var path = require('path');

var server = http.createServer(function(req, res) {
    console.log('req starting...');

    var filePath = '.' + req.url;
    if (filePath == './')
        filePath = './index.html';

    console.log(filePath);

    var contentType = 'text/html';
    var extname = path.extname(filePath);
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
    }

    fs.readFile(filePath, function(error, content) {
        if (error) {
            res.writeHead(500);
            console.log("error: "+error.code);
            res.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
        }
        else {
            res.writeHead(200, { 'Content-Type': contentType });
            res.end(content, 'utf-8');
        }
    });
});

server.listen(5000);

console.log('Node.js web server at port 5000 is running');
