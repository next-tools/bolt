# BOLT - An interactive compiler writing environment

I wanted to provide a simple framework of two windows where a programmer can
enter input on the left window, send that source through a javascript function,
which then provides output to the right window.

The reason for this wish is that I realised this would the first step into
helping writing a compiler where the source of my language can be written on the
left and I can see machine code or any other output on the right.

The main inspiration for this way of working is, of course, Matt Godbolt's
compiler explorer (hence its name). But soon after that realisation, a further
realisation occurred that this kind of framework would be useful for any kind of
work in developing a compiler or transpiler.

Now, I am not a web developer but soon discovered the amazing Ace editor, which
is a full blown text editor implementation written in JavaScript. But I was
still a long way off from creating a 2 window user interface, with each
containing an instance of that Ace editor. Fortunately for me, D. 'Xalior'
Rimron-Soutter offered to get me started allowing me the simple task of
reorganisation and adding some simple compiler examples. So thanks D!

Hopefully, something like this project can help teachers inform students how to
write compilers.

## Installation

Just git clone and initialise/update submodules this source code and run `node
server.js`. This will start the server and open a port (default `5000`) for your
web browser to connect to (connect to `localhost:5000`).

## Use

The framework calls the `compile` function in `compile.js`. Just rewrite this
function to do whatever you want. The function takes a string parameter (the
whole input source) and must return another string (the output).

Additionally, the framework calls `editorSetup` and `outputSetup` in
`editor-setup.js` to give you the chance to mess with the Ace editor's settings.

## Examples

The `master` branch is intended to always contain an extremely simple example
(reverses the input) so you can fork that, replace it and get going. However,
I've added an `example` branch will populate the `compile` function to do
something more interesting. I may add other branches for further work.
