function editorSetup(editor) {
    editor.setOptions({
        mode: "ace/mode/c_cpp",
        theme: "ace/theme/dracula",
    });
}

function outputSetup(output) {
    output.setOptions({
        mode: "ace/mode/text",
        theme: "ace/theme/dawn",
    });
}
