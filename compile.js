function compile(src) {
    const splitString = src.split("");
    const reverseArray = splitString.reverse();
    return reverseArray.join("");
}
